interface(echo=0):
macro(W=LambertW):
test:=proc(f,x,k,p,eps)
local eq, n, v1, v2;
   eq:=equivalent(f,x,n,p);
   v1:=evalf(subs(n=k,eval(eq,O=0)));
   v2:=evalf(coeff(eval(convert(series(f,x,k+5),polynom),O=0),x,k));
   # lprint(abs((v1-v2)/v1))
   if abs((v1-v2)/v1)<eps then okay else f,v1,v2 fi
end:

# -*-maple-*-
unprotect('shake');
shake:=proc (expr)
local ampl, res;
	option 
	    `Copyright (c) 1991 by the University of Waterloo. All rights reserved.`;
	if nargs=2 and type(args[2],integer) and args[2]>0 then ampl:=args[2]
	else ampl:=Digits fi;
	if indets(expr,name) minus ({constants} minus {I}) = {} then
		`evalr/shake`(expr,ampl)
	elif type(expr,name) then 'INTERVAL'(-infinity..infinity)
	else res:=`evalr/evalr`(map(shake,expr,ampl),false);
	    subsop(4=NULL,op(`evalr/evalr`)); res
	fi
end: # shake
protect(shake);
#savelib('shake'):

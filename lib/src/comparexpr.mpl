# -*-maple-*-
#####################################################
#	    
#				  COMPAREXPR
#
#
####################################################

comparexpr:=proc(expr1,expr2)
local s;
option `Copyright Bruno Salvy, INRIA, France`;
    s:=signum(0,expr1-expr2,0); 
    if s=1 then `>`
    elif s=-1 then `<`
    elif s=0 then `=`
    elif type(s,specfunc(anything,signum)) 
	and expr2<>0 # second test to avoid infinite loop
	then comparexpr(evalf(expr1-expr2),0)
    else
	userinfo(3,'equivalent',`Warning: assumption made`,expr1=expr2);
	`=`
    fi
end:

#savelib( comparexpr);

# These two procedures are used by equivalent

`evalc/QuasiLog`:=proc (expr)
    evalc(ln(1/(1-expr)))
end: # `evalc/QuasiLog`

#savelib( `evalc/QuasiLog`);

# These two procedures are used by equivalent
`evalc/QuasiInverse`:=proc (expr)
    evalc(1/(1-expr))
end: # `evalc/QuasiInverse`

#savelib( `evalc/QuasiInverse`);

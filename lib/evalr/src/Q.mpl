# This is only used in the Luo system
`evalr/QuasiInverse`:=proc(rg,exact)
    `evalr/pow`(`evalr/add`([1..1],`evalr/prd`([-1..-1],rg,exact)),-1,exact)
end: # `evalr/QuasiInverse`

#savelib( `evalr/QuasiInverse`);

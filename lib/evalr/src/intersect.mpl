# -*-maple-*-
# $Source: /u/maple/research/lib/evalr/src/RCS/intersect,v $
##
##    Title:    `evalr/intersect`
##    Created:   Aug 88
##    Author:    Bruno Salvy
##      <bsalvy@watmum>
##

`evalr/intersect`:=proc(liste)
local i,tmp;
option 
    `Copyright (c) 1990 by the University of Waterloo. All rights reserved.`;
if nops(liste)>=2 then
   tmp:=op(1,liste);
   for i from 2 to nops(liste) do
      if member(evalr(Signum(op([i,1],liste)-op(2,tmp))),{0,1}) or
         member(evalr(Signum(op([i,2],liste)-op(1,tmp))),{-1,0}) then
         RETURN(NULL)
      else tmp:=evalr(max(op([i,1],liste),op(1,tmp)))..
           evalr(min(op([i,2],liste),op(2,tmp)))
      fi
   od
else
   liste
fi
end:

#savelib('`evalr/intersect`'):

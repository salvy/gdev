# This is only used in the Luo system
`evalr/QuasiLog`:=proc(rg,exact)
    `evalr/ln`(`evalr/pow`(`evalr/add`([1..1],
	`evalr/prd`([-1..-1],rg,exact)),-1,exact),exact)
end: # `evalr/QuasiLog`
#savelib( `evalr/QuasiLog`);

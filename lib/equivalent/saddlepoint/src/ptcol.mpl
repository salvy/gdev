# -*-maple-*-
`equivalent/saddlepoint/pointcol`:=proc(f,x,sing,p)
local aux, aux2;
    aux:=max(3,p);
    if sing<>infinity then
	aux2:=`dev/implicit`(`dev/dev`(subs(x=sing*(1-1/_Xasy),x*diff(f,x)/f),
	    aux,aux),[1,1,-1,0,infinity],aux)
    else
	aux2:=`dev/implicit`(`dev/dev`(subs(x=_Xasy,x*diff(f,x)/f),aux,aux),
	    [1,1,-1,0,infinity],aux)
    fi;
    if aux2<> FAIL then
	if sing=infinity then aux2
	else
	    map(`dev/multbyreal`,map(`dev/add`,map(`dev/pow`,
		aux2,-1,p),-1),-sing)
	fi
    else FAIL
    fi
end:

#savelib( `equivalent/saddlepoint/pointcol`);

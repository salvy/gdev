# -*-maple-*-
##################################################
#
#				    SELLE
#
#   Apply saddle-point method, the user is asked to
# check the validity by himself.
#
##################################################

`equivalent/saddlepoint/selle`:=proc(fct,var,sing)
local u,h,x,n;
    x:=var;
    h:=ln(fct)-(n+1)*ln(x);
if assigned(_EnvSolveSaddlePoint) and _EnvSolveSaddlePoint=false then
    u:=[]
else
    u:=[solve(diff(h,x)=0,x)];
fi;
    `equivalent/saddlepoint/saddle`(u,sing,fct,var,true,n);
    print(`Assuming the saddle point method is valid in this context,`);
    print(`the result is:`);
    if sing<>infinity then
	_NBSADDLEPOINTS,`dev/reduce`(`dev/dev`(
	    subs(x=sing*(1-1/_Xasy),fct/(2*Pi)^(1/2)/diff(h,x,x)^(1/2)),1,1),2)
    else
	_NBSADDLEPOINTS,`dev/reduce`(`dev/dev`(subs(x=_Xasy,fct/x^n/
	    (2*Pi)^(1/2)/diff(h,x,x)^(1/2)),1,1),2)
    fi
end:
#savelib( `equivalent/saddlepoint/selle`);

# -*-maple-*-
`infsing/QuasiInverse`:=proc (fct,var,singmin) # we assume the coeff to be >=0
local val;
    val:=`infsing/trie`(`infsing/infsolve`(op(1,fct)-1,var,true,singmin),[infinity],fct,var,
	'polar','polynom');
    if val[1][1]=infinity then infsing(op(1,fct),var,singmin)
    else [val,true]
    fi
end: # `infsing/QuasiInverse`

#savelib( `infsing/QuasiInverse`);

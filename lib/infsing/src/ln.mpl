# -*-maple-*-
##
##    Title:	`infsing/ln`
##    Created:	Thu May 23 18:58:45 1991
##    Author:	Bruno Salvy
##	<salvy@rully.inria.fr>
##
## Description: smallest singularities of fct in the variable var
##  with modulus larger than singmin, p being the order of intermediate
##  expansions
`infsing/ln`:=proc (fct,var,sm)
local locval, val, singmin, expr;
    singmin:=sm;
    expr:=op(fct);
    do
	locval:=infsing(expr,var,singmin);
	val:=[`infsing/trie`(`infsing/infsolve`(expr,var,false,singmin),locval[1],fct,var,
	    'log','log'),false];
	if locval[1][1]<>infinity then
	    if val[1][1]=infinity then
		singmin:=abs(locval[1][1])
	    else
		break
	    fi
	else
	    break
	fi
    od;
    val
end: # `infsing/ln`

#savelib( `infsing/ln`);

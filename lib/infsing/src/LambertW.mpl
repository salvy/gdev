# -*-maple-*-
`infsing/LambertW`:=proc (fct,var,Singmin)
local locval,val,singmin;
    singmin:=Singmin;
    do
	locval:=infsing(op(1,fct),var,singmin);
	val:=[`infsing/trie`(`infsing/infsolve`(op(1,fct)+exp(-1),var,false,singmin),
		locval[1],fct,var,'algebraic',locval[2]),false];
	if locval[1][1]<>infinity then
	    if val[1][1]=infinity then
		singmin:=abs(locval[1][1])
	    else
		break
	    fi
	else
	    break
	fi
    od;
    val
end: # `infsing/W`

#savelib( `infsing/LambertW`);

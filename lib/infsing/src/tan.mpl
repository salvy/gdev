# -*-maple-*-
`infsing/tan`:=proc (fct, var, Singmin)
local singmin, locval, val;
    singmin:=Singmin;
    do
	locval:=infsing(op(1,fct),var,singmin);
	val:=[`infsing/trie`(`infsing/infsolve`(cot(op(1,fct)),var,false,singmin),
		locval[1],fct,var,'polar',locval[2]),false];
	if locval[1][1]<>infinity then
	    if val[1][1]=infinity then singmin:=abs(locval[1][1]) else break fi
	else break fi
    od;
    val
end: # `infsing/tan`

#savelib( `infsing/tan`);

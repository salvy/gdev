`conjugate/RootOf`:=proc (x,y)
    if nargs=2 and type(y,'complex(numeric)') then RootOf(x,conjugate(y))
    else 'conjugate'(RootOf(args)) fi
end: # `conjugate/RootOf`

#savelib(`conjugate/RootOf`);

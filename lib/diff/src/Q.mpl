##    -*-Maple-*-
##
##    Title: 	diff/L and diff/Q
##    Author: 	Bruno Salvy <Bruno.Salvy@inria.fr>
##
## These two procedures are used by equivalent

`diff/QuasiInverse`:=proc(a,x) option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
QuasiInverse(a)^2*diff(a,x) end:

#savelib(`diff/QuasiInverse`);

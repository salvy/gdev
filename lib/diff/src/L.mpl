##    -*-Maple-*-
##
##    Title: 	diff/L and diff/Q
##    Author: 	Bruno Salvy <Bruno.Salvy@inria.fr>
##
## These two procedures are used by equivalent

`diff/QuasiLog`:=proc(a,x) option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
diff(a,x)/(1-a) end:

#savelib( `diff/QuasiLog`);

# -*-maple-*-
##
##    Title: 	dev/abs
##    Created:	1987
##    Author: 	Bruno Salvy
##		<Bruno.Salvy@inria.fr>
##
## Description: expansion of a modulus

`dev/abs`:=proc(dev,n)
local impart, sig;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if type(dev,undefined) then undefined
    elif not type(dev,list) then evalc(abs(dev))
    else
	impart:=`dev/impart`(dev);
	if not has(impart,I) then
	    sig:=evalr(Signum(`dev/lcoeff`(dev)));
	    if sig=1 then dev
	    elif sig=-1 then `dev/multbyreal`(dev,-1)
	    elif sig=0 then [0]
	    else FAIL
	    fi
	else
	    `dev/pow`(`dev/add`(`dev/pow`(`dev/realpart`(dev),2,n),
				`dev/pow`(subs(I=1,impart),2,n)),1/2)
	fi
    fi
end:


#savelib(`dev/abs`);

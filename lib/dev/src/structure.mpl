##    -*-Maple-*-
##
##    Title:	STRUCTURE OF THE GENERALIZED ASYMPTOTIC EXPANSIONS
##    Created:	Mon Dec 26 11:51:50 1988
##    Author:	Bruno Salvy
##	<salvy@poly.polytechnique.fr>
##    Modified:	    Sun Aug 13 12:01:52 1989
##    Author:	Bruno Salvy
##    Modification: exprseq instead of product and index as first elt instead
##		    of X[index] everywhere.
##
##     A generalized asymptotic expansion is:
##	 * either undefined
##       * or a constant (coding itself)
##	 * or a list coding a sum,
##		the first term is the index of the variable which may be 
##		an integer or a rational number:
##		 1  --> 1/x
##		 2  --> 1/log(x)
##		 3  --> 1/log(log(x)) ...
##		 0  --> 1/exp(x)
##		 -1 --> 1/exp(exp(x)) ...
##	      	non-integer indices are determined by dev/indexify
##	    the other terms encode products
##		      C*X[i]^alpha, where
##		 - i is the index
##		 - alpha is real
##		 - C is an asymptotic expansion whose lowest monomial is
##		       smaller than X[i] or a constant term which may be 
##		       a bounded function of any X[i] 
##	       This product is encoded as an exprseq of 2 elements: C,alpha
##	    the last term is the power of the remainder (to appear in the O())
##		infinity means that the expansion is exact.
##
quit

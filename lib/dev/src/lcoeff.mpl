# -*-maple-*-
##
##    Title: 	dev/lcoeff
##    Created:	1991
##    Author: 	Bruno Salvy
##		<salvy@rully.inria.fr>
##
## Description: leading coefficient of an expansion

`dev/lcoeff`:=proc(dev)
local u;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if not type(dev,list) then dev
    elif nops(dev)=3 then
	if not type(op(2,dev),list) then FAIL
	else `dev/lcoeff`(op(2,dev))
	fi
    else
	u:=dev;
	while type(u,list) do u:=op(2,u) od;
	u
    fi
end:

#savelib( `dev/lcoeff`);

 # -*-maple-*-
##
##    Title: 	dev/divide
##    Created:	1988
##    Author: 	Bruno Salvy
##		<bruno.salvy@inria.fr>
##
## Description: This procedure is only used by the Luo system.

`dev/divide`:=proc(dev1,dev2,p)
local res;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    _EnvXasyinteger:=true;
    if not has(dev1,_saddlepoint) then
	res:=`dev/prd`(dev1,`dev/pow`(dev2,-1,p))
    else
	res:=[_saddlepoint,dev1[2],`dev/prd`(dev1[3],`dev/pow`(dev2[3],-1,p))]
    fi;
    RETURN(res)
end:

#savelib( `dev/divide`);

# -*-maple-*-
##
##    Title: 	dev/instanc
##    Created:	1989
##    Author: 	Bruno Salvy
##		<bruno.salvy@inria.fr>
##
## Description: translates from the internal representation into a sum of
## products.

`dev/instanc`:=proc(expr)
local i, res, lim, var;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if not type(expr,list) then res:=expr
    else
	if expr[nops(expr)]<>infinity then
	    lim:=nops(expr)
	else
	    lim:=nops(expr)-2
	fi;
	res:=0;
	var:=_Xasytab[expr[1]];
	for i from 2 by 2 to lim do
	    if type(expr[i],list) then
		res:=res+`dev/instanc`(expr[i])*var^expr[i+1]
	    else
		res:=res+expr[i]*var^expr[i+1]
	    fi
	od
    fi
end:

#savelib( `dev/instanc`);

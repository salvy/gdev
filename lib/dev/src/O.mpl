# -*-maple-*-
##
##    Title: 	`dev/O`
##    Created:	1987
##    Author: 	Bruno Salvy
##		<Bruno.Salvy@inria.fr>
##

`dev/O`:=proc(dev)
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if type(dev,undefined) then undefined
    elif not type(dev,list) then 1
    else [dev[1],`dev/O`(dev[2]),dev[3]]
    fi
end:

#savelib(`dev/O`);

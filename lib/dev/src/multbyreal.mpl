# -*-maple-*-
##
##    Title: 	dev/multbyreal
##    Created:	Tue Jul 12 11:25:10 1994
##    Author: 	Bruno Salvy
##		<salvy@rully.inria.fr>
##
## Description: external product

`dev/multbyreal`:=proc(tree,r)
local i,res;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if r=0 then 0
    elif type(tree,undefined) then undefined
    elif not type(tree,list) then r*tree
    else
	for i from 2 by 2 to nops(tree) do
	    res[i]:=`dev/multbyreal`(tree[i],r),tree[i+1]
	od;
	[tree[1],seq(res[2*i],i=1..iquo(nops(tree),2))]
    fi
end:

#savelib( `dev/multbyreal`);

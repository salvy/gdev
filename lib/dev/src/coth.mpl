`dev/coth`:=proc (u,n)
local fact, i, example, ifact, init, inter, sig;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if type(u,undefined) then
	RETURN(undefined)
    fi;
    if not type(u,list) then
	init:=traperror(coth(u[1]));
	if init=lasterror then 
	    RETURN(undefined)
	else
	    RETURN(init)
	fi
    fi;
    sig:=evalr(Signum(u[3]));
    if sig=1 then
	fact:=`dev/pow`(u,2,n);
	example:=[1/3,0,-1/45,1,2/945,2];
	ifact:=4/45;
	for i from 3 to n-1 do
	    ifact:=ifact*2/(2*i+1)/(i+1);
	    example:=[op(example),ifact*bernoulli(2*i+2),i]
	od;
	RETURN(`dev/add`(`dev/pow`(u,-1,n),`dev/prd`(u,`dev/endofdev`(fact,
	    n-1,example))))
    elif sig=-1 then
	sig:=evalr(Signum(`dev/lcoeff`(u)));
	if sig=1 then
	    RETURN(`dev/endofdev`(`dev/exp`(`dev/multbyreal`(u,-1),n),n,
		[1,0,seq(op([2,i]),i=1..n)]))
	else
	    RETURN(`dev/endofdev`(`dev/exp`(u,n),n,[-1,0,seq(op([-2,i]),i=1..n)
	    ]))
	fi
    elif sig=0 then # same comment as above
	init:=`dev/coth`(u[2],n);
	inter:=`dev/coth`(subsop(2=NULL,3=NULL,u),n);
	RETURN(`dev/prd`(`dev/add`(1,`dev/prd`(init,inter)),
	    `dev/pow`(`dev/add`(init,inter),-1,n)))
    else
	ERROR(FAIL)
    fi;
end: # `dev/coth`
#savelib(`dev/coth`);

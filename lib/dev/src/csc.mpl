`dev/csc`:=proc(u,n)
local fact,i, init, newres, res, tomult, sig;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if not type(u,list) then
	init:=traperror(csc(u));
	if init=lasterror or has(init,infinity) then RETURN(undefined)
	else RETURN(init)
	fi
    fi;
    sig:=evalr(Signum(u[3]));
    if sig=1 then
	fact:=`dev/pow`(u,2,n);
	tomult:=u;
	res:=0;
	newres:=`dev/add`(`dev/pow`(u,-1,n),`dev/multbyreal`(u,1/6));
	for i from 2 to n+1 while newres<>res do	
	    res:=newres;
	    tomult:=`dev/prd`(tomult,fact);
	    newres:=`dev/add`(res,`dev/multbyreal`(tomult,
		(-1)^(i-1)*2*(2^(2*i-1)-1)*bernoulli(2*i)/(2*i)!))
	od;
	RETURN(`dev/reduce`(newres,n))
    elif sig=-1 then RETURN(undefined)
    elif sig=0 then
	RETURN(`dev/pow`(`dev/cossin`(u,n)[2],-1,n))
    else ERROR(FAIL)
    fi
end:
#savelib(`dev/csc`);

# -*-maple-*-
##
##    Title: 	dev/binomial
##    Created:	1987
##    Author: 	Bruno Salvy
##		<bruno.salvy@inria.fr>
##
## Description: expansion of binomials

`dev/binomial`:=proc (u,v,n)
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if type(u,undefined) or type(v,undefined) then undefined
    else 
	`dev/prd`(`dev/GAMMA`(`dev/add`(u,1),n),
	`dev/pow`(`dev/prd`(`dev/GAMMA`(`dev/add`(v,1),n),
	`dev/GAMMA`(`dev/add`(u,`dev/add`(`dev/multbyreal`(v,-1),1)),n)),-1,n))
    fi
end: # `dev/binomial`

#savelib( `dev/binomial`);

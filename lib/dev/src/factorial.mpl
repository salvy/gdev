# -*-maple-*-
##
##    Title: 	dev/factorial
##    Created:	1988
##    Author: 	Bruno Salvy
##		<salvy@rully.inria.fr>
##
## Description: expansion of a factorial

`dev/factorial`:=proc(dev,n)
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    `dev/GAMMA`(`dev/add`(dev,1),n)
end:

#savelib( `dev/factorial`);

`dev/arctanh`:=proc (u,n)
local fact, i, example, x, j, k, init, sig;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if type(u,undefined) then
	RETURN(undefined)
    fi;
    if not type(u,list) then
	init:=traperror(arctanh(u));
	if init=lasterror then RETURN(undefined)
	else RETURN(init)
	fi
    fi;
    sig:=evalr(Signum(u[3]));
    if sig=1 then
	RETURN(`dev/prd`(u,`dev/endofdev`(`dev/pow`(u,2,n),n,[seq(op(
	    [1/(2*i+1),i]),i=0..n)])))
    elif sig=-1 then
	RETURN(undefined)
    elif sig=0 then
	init:=`dev/arctanh`(u[2],n);
	if `dev/length`(init)>n and init[nops(init)]<>infinity then RETURN(init)
	else
	    example:=[init,0];
	    j:=arctanh(x);
	    k:=1;
	    for i to n do
		j:=diff(j,x);
		k:=k/i;
		example:=[op(example),subs(x=u[2],j)*k,i]
	    od;
	    fact:=subsop(2=NULL,3=NULL,u);
	    RETURN(`dev/endofdev`(fact,n,example))
	fi
    else
	ERROR(FAIL)
    fi;
end: # `dev/arctanh`
#savelib(`dev/arctanh`);

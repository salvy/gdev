# -*-maple-*-
##
##    Title:	evenpart
##    Created:	Thu Sep 19 20:35:56 1991
##    Author:	Bruno Salvy
##	<salvy@rully.inria.fr>
##
## Description: returns the evenpart of an expansion. The argument should
## be a Puiseux series.

`dev/evenpart`:=proc (dev)
local a, n, i;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if not type(dev,list) then dev # a constant function is even
    elif op(1,dev)<>1 then ERROR(`a Puiseux series is expected`)
    else
	n:=iquo(nops(dev),2)-1;
	for i to n do
	    if type(op(2*i+1,dev),even) then a[i]:=op(2*i,dev),op(2*i+1,dev)
	    else a[i]:=NULL fi
	od;
	[op(1,dev),seq(a[i],i=1..n),op(2*n+2,dev),op(2*n+3,dev)]
    fi
end: # `dev/evenpart`

#savelib( `dev/evenpart`);

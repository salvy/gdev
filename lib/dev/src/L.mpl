# -*-maple-*-
##
##    Title: 	dev/Q, dev/L
##    Created:	1988
##    Author: 	Bruno Salvy
##		<bruno.salvy@inria.fr>
##
## Description:  This is only used in the Luo system.
## These functions define the expansions
## of the quasi-inverse (Q) and of the quasi-logarithm (L).
## Few test are performed, since the input is supposed to be ok.

`dev/QuasiLog`:=proc(u,n)
local i, init, sig;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if u[3]=0 then
	if not type(u[2],list) and zerotest(u[2]-1)<>false then
#	    if u[2]=1 then
		`dev/multbyreal`(`dev/ln`(`dev/multbyreal`(
		    subsop(2=NULL,3=NULL,u),-1),n),-1)
#	    else
#		`dev/multbyreal`(`dev/ln`(`dev/multbyreal`(
#		    subs(u[2]=1,subsop(2=NULL,3=NULL,u)),-1),n),-1)
#	    fi
	elif not type(u[2],list) then
	    init:=1/(1-u[2]);
    	    `dev/add`(ln(init),`dev/QuasiLog`(`dev/multbyreal`(
		subsop(2=NULL,3=NULL,u),init),n))
	else
	    `dev/multbyreal`(`dev/ln`(`dev/add`(`dev/multbyreal`(u,-1),1),n),-1)
	fi
    else
	sig:=evalr(Signum(u[3]));
	if sig=-1 then
	    `dev/multbyreal`(`dev/ln`(`dev/add`(`dev/multbyreal`(u,-1),1),n),-1)
	elif sig=FAIL then ERROR(FAIL)
	else
	    `dev/endofdev`(u,n+1,map(op,[seq([1/i,i],i=1..n+2)]))
	fi
    fi
end: #`dev/QuasiLog`

#savelib( `dev/QuasiLog`);

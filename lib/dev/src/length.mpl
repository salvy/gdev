# -*-maple-*-
##
##    Title:	`dev/length`
##    Created:	Sun Aug 13 12:44:26 1989
##    Author:	Bruno Salvy
##	<bsalvy@watmum.waterloo.edu>
##
## Description: Returns the number of terms in an expansion.

`dev/length`:=proc (dev)
local i;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if not type(dev,list) then 1
    else add(`dev/length`(dev[2*i]),i=1..iquo(nops(dev),2))
    fi
end: # `dev/length`

#savelib( `dev/length`);

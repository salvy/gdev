# -*-maple-*-
##
##    Title: 	dev/sign
##    Created:  1988
##    Author: 	Bruno Salvy
##		<salvy@rully.inria.fr>
##
## Description: sign of an expansion


`dev/sign`:=proc(dev)
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if not type(dev,list) then evalr(Signum(dev))
    else `dev/sign`[dev[2]]
    fi
end:

#savelib( `dev/sign`);

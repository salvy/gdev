# -*-maple-*-
##
##    Title: 	dev/reduce
##    Created:	1989
##    Author: 	Bruno Salvy
##		<salvy@rully.inria.fr>
##
## Description: Given an expansion dev and an integer k, 
## suppress the terms of order greater than k.


`dev/reduce`:=proc(dev,n)
local i, rest, result, l;
option `Copyright Bruno Salvy, INRIA Rocquencourt, France`;
    if not type(dev,list) then dev
    elif not hastype({op(dev)},list) then [op(1..min(2*n+3,nops(dev)),dev)]
    elif `dev/length`(dev)<=n+1 then dev
    else
	rest:=n+1;
	for i from 2 by 2 to nops(dev) while rest<>0 do
	    if not type(dev[i],list) then
		result[i]:=dev[i],dev[i+1];
		rest:=rest-1
	    else
		l:=`dev/length`(dev[i]);
		if l<=rest then
		    result[i]:=dev[i],dev[i+1];
		    rest:=rest-l
		else 
		    result[i]:=`dev/reduce`(dev[i],rest-1),dev[i+1];
		    rest:=0
		fi
	    fi
	od;
	if (i=2 or (i=4 and rest=0)) and result[2][2]=0 then result[2][1]
	elif rest=0 then [dev[1],seq(result[2*i],i=1..iquo(i,2)-1)]
	else [dev[1],seq(result[2*i],i=1..iquo(i,2))]
	fi
    fi
end:
 
#savelib( `dev/reduce`);

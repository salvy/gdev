helptool := proc(the_file, the_topic, the_localization, the_hdb)
  local N, the_line, line, common ;
  N := 0 ;
  the_line := readline(the_file) ;
  while the_line<>0 do
    N := N+1 ;
    line[N] := the_line ;
    the_line := readline(the_file)
  end do ;
  common := insert, topic=the_topic, library=the_hdb ;
  print(INTERFACE_HELP(common,
    op(select(has, the_localization, [parent, aliases])),
    text=TEXT(seq(line[i], i=1..N)))) ;
  print(INTERFACE_HELP(common, op(the_localization))) ;
  NULL
end :
